#include <string>
#include <iostream>
#include <vector>
#include <FaceDetector.h>
#include <opencv4/opencv2/opencv.hpp>

FaceDetector::FaceDetector(const std::string& configFile, const std::string& weightFile)
    : confidenceThreshold_(0.5)
    , imageHeight_(480)
    , imageWidth_(640)
    , scaleFactor_(0.007843)
    , meanValues_({127.5, 127.5, 127.5}) // 104., 177.0, 123.0 for opencv model
{
    // Note: The varibles MODEL_CONFIGURATION_FILE and MODEL_WEIGHTS_FILE are passed in via cmake
    network_ = cv::dnn::readNetFromCaffe(configFile, weightFile);

    if (network_.empty()) {
        std::cout << "Failed to load network file" << std::endl;
    }
}

std::vector<cv::Rect> FaceDetector::detect(const cv::Mat& frame)
{
    cv::Mat inputBlob = cv::dnn::blobFromImage(frame, scaleFactor_, cv::Size(imageWidth_, imageHeight_),
                                               meanValues_, false, false);
    network_.setInput(inputBlob, "data");
    cv::Mat detection = network_.forward("detection_out");
    cv::Mat detectionMatrix(detection.size[2], detection.size[3], CV_32F, detection.ptr<float>());

    std::vector<cv::Rect> faces;

    for (int i = 0; i < detectionMatrix.rows; i++) {
        float confidence = detectionMatrix.at<float>(i, 2);

        if (confidence < confidenceThreshold_) {
            continue;
        }
        int xLeftBottom = static_cast<int>(detectionMatrix.at<float>(i, 3) * frame.cols);
        int yLeftBottom = static_cast<int>(detectionMatrix.at<float>(i, 4) * frame.rows);
        int xRightTop   = static_cast<int>(detectionMatrix.at<float>(i, 5) * frame.cols);
        int yRightTop   = static_cast<int>(detectionMatrix.at<float>(i, 6) * frame.rows);

        faces.emplace_back(xLeftBottom, yLeftBottom, (xRightTop - xLeftBottom), (yRightTop - yLeftBottom));
    }

    return faces;
}
