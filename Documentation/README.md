This documentation is about how to calibrate stereo camera and measure the distance of detected objects.

# Software dependency

#### Qt 5
There are different ways to install Qt library. You can follow [Install Qt 5 on Ubuntu](https://wiki.qt.io/Install_Qt_5_on_Ubuntu) or [Building Qt 5 from Git](https://wiki.qt.io/Building_Qt_5_from_Git#Getting_the_source_code)

#### OpenCV 4.x
Follow [Installation OpenCV](https://docs.opencv.org/3.4/d7/d9f/tutorial_linux_install.html) to install OpenCV. It is recommended to install [OpenCV contrib](https://wiki.qt.io/Building_Qt_5_from_Git#Getting_the_source_code) as well. 
To compile OpenCV you can use the following commands for debug and release version.
```sh
cmake -DOPENCV_ENABLE_NONFREE:BOOL=ON -DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX=/usr/local -DOPENCV_EXTRA_MODULES_PATH=<<path to opencv_contrib/modules>> ..
cmake -DOPENCV_ENABLE_NONFREE:BOOL=ON -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/usr/local -DOPENCV_EXTRA_MODULES_PATH=<<path to opencv_contrib/modules>> ..
```
The enviroment variable "OpenCV_DIR" should be set to paht of OpenCV library e.g. "/home/opencv/build_Release/".

#### CMake 3.8+
Follow [Installing CMake](https://cmake.org/install/) to install OpenCV.

# Quick start
Compile and build project using following commands.
```sh
mkdir build
cd build
cmake ..
make
```
The binaries "takephoto", "calibration" and "VideoStream" will be then generated in directory "build".

Run "takephoto" to take photos for the calibration. The path of two cameras should be given as inputs. e.g.
```sh
./takephoto /dev/video0 /dev/video2 
```
The images will be saved in foler "image" located at the root of project. You can edit the variables defined in file "takephoto.cpp" to change the file name, file extention and so on.
The chess board with size 25mm-8*6 is required, which is save as "Checkerboard-A4-25mm-8x6.pdf". Just print it in format of A4 and glue it to the flat hard board. You can also use your own checkboard. If the size of your board is not identical, you must adjust the corresponding variables in file "calibration.cpp".
![Chess Board](ChessBoard.jpg "Title")

Run "calibration" to calculate the data for the camera calibration.
```sh
./calibration
```
The intrinsic and extrinsic parameters of cameras will be saved in files "left.yml", "right.yml" and "stereo.yml" in folder "camera_data" located at the root of project. Note: if you edit the path or name of the images for the calibration, you need to adjust the corresponding variables in file "calibration.cpp".

More details about camera calibration can be seen in [Camera Calibration using OpenCV](https://learnopencv.com/camera-calibration-using-opencv/) and [Camera Calibration OpenCV](https://docs.opencv.org/3.4/dc/dbb/tutorial_py_calibration.html)

Run "VideoStream" to launch the main application. The application load the extrinsic parameters in file "stereo.yml" at the initial phase. The GUI shows then the undistorted vedio streams.
```sh
./VideoStream
```
