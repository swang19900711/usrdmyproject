#pragma once

#include <opencv4/opencv2/dnn.hpp>

class FaceDetector
{
public:
    explicit FaceDetector(const std::string& configFile, const std::string& weightFile);

    std::vector<cv::Rect> detect(const cv::Mat& frame);

    void draw_rectangles_around_detected_faces(const std::vector<cv::Rect>& detected_faces, cv::Mat image) const;

private:
    cv::dnn::Net network_;

    const float confidenceThreshold_;

    const int imageHeight_;

    const int imageWidth_;

    const double scaleFactor_;

    const cv::Scalar meanValues_;
};
