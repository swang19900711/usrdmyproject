#include "mainwindow.h"
#include <algorithm>
#include <iostream>
#include <list>
#include <sstream>
#include "ui_mainwindow.h"
#include <opencv2/xfeatures2d.hpp>

using namespace cv;
using namespace std;

void onMouseLeftDisparity(int event, int x, int y, int flags, void* params)
{
    Mat* image = reinterpret_cast<Mat*>(params);
    switch (event) {
    case EVENT_LBUTTONDOWN:
        float pixelValue = static_cast<float>(image->at<float>(Point(x, y)));
        qInfo() << "The disparity value at ( " << x << ", " << y << ") : " << pixelValue;
        break;
    }
}

void onMouseLeftPos(int event, int x, int y, int flags, void* params)
{
    Mat* image = reinterpret_cast<Mat*>(params);
    switch (event) {
    case EVENT_LBUTTONDOWN:
        int pixelValue = static_cast<int>(image->at<uchar>(Point(x, y)));
        qInfo() << "The pixel value at left ( " << x << ", " << y << ") : " << pixelValue;
        break;
    }
}

void onMouseRightPos(int event, int x, int y, int flags, void* params)
{
    Mat* image = reinterpret_cast<Mat*>(params);
    switch (event) {
    case EVENT_LBUTTONDOWN:
        int pixelValue = static_cast<int>(image->at<uchar>(Point(x, y)));
        qInfo() << "The pixel value at right ( " << x << ", " << y << ") : " << pixelValue;
        break;
    }
}

MainWindow::MainWindow(QWidget* parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , faceDetector("../data/MobileNetSSD_deploy.prototxt", "../data/MobileNetSSD_deploy.caffemodel") // deploy.prototxt res10_300x300_ssd_iter_140000_fp16.caffemodel MobileNetSSD_deploy.prototxt MobileNetSSD_deploy.caffemodel
    , skipFrame(2)
{
    ui->setupUi(this);

    ui->graphicsViewLeft->setScene(new QGraphicsScene(this));
    ui->graphicsViewLeft->scene()->addItem(&pixmapLeft);
    ui->graphicsViewRight->setScene(new QGraphicsScene(this));
    ui->graphicsViewRight->scene()->addItem(&pixmapRight);
    // set default path of camera
    ui->videoEditLeft->setText("/dev/video0");
    ui->videoEditRight->setText("/dev/video2");

    // load calibration data for stereo cameras
    loadCalibrationData("../camera_data/stereo.yml");

    // load training data for object detection
    if (!cascade.load("../data/haarcascade_profileface.xml")) { // haarcascade_frontalface_alt2 haarcascade_profileface
        std::cerr << "Could not load cascade data!" << std::endl;
        return;
    }
    defaultHog.setSVMDetector(HOGDescriptor::getDefaultPeopleDetector());

    // for Debug
    ui->graphicsViewTEST->setScene(new QGraphicsScene(this));
    ui->graphicsViewTEST->scene()->addItem(&pixmapTest);
    TESTVarSetDefault();
    TESTVarUpdate();
}
void MainWindow::TESTVarSetDefault()
{
    ui->TESTSGBMValue1->setValue(1.2);  // scaleFactor
    ui->TESTSGBMValue2->setValue(6);    // minNeighbors
    ui->TESTSGBMValue3->setValue(50);   // minSize
    ui->TESTSGBMValue4->setValue(0.5);  // expand factor
    ui->TESTSGBMValue5->setValue(3);    // max of deviation of disparity y
    ui->TESTSGBMValue6->setValue(5000); // hessianThreshold
    ui->TESTSGBMValue7->setValue(4);    // nOctaves
    ui->TESTSGBMValue8->setValue(3);    // nOctaveLayers
    ui->TESTSGBMValue9->setValue(0.8);  // distance selector
    ui->TESTSGBMValue10->setValue(6);   // skpFrame
}
void MainWindow::TESTVarUpdate()
{
    TESTSSGBVar1  = ui->TESTSGBMValue1->value();
    TESTSSGBVar2  = ui->TESTSGBMValue2->text().toInt();
    TESTSSGBVar3  = ui->TESTSGBMValue3->text().toInt();
    TESTSSGBVar4  = ui->TESTSGBMValue4->value();
    TESTSSGBVar5  = ui->TESTSGBMValue5->text().toInt();
    TESTSSGBVar6  = ui->TESTSGBMValue6->text().toInt();
    TESTSSGBVar7  = ui->TESTSGBMValue7->text().toInt();
    TESTSSGBVar8  = ui->TESTSGBMValue8->text().toInt();
    TESTSSGBVar9  = ui->TESTSGBMValue9->value();
    TESTSSGBVar10 = ui->TESTSGBMValue10->text().toInt();
}

bool MainWindow::openCamera(const QString& videoPath, VideoCapture& videoCap)
{
    auto isCamera    = false;
    auto cameraIndex = videoPath.toInt(&isCamera);
    if (isCamera) {
        if (!videoCap.open(cameraIndex)) {
            QString msg = QString("Make sure you entered a correct camera index: ") + videoPath;
            QMessageBox::critical(this, "Camera Error", msg);
            return false;
        }
    } else {
        if (!videoCap.open(videoPath.trimmed().toStdString())) {
            QString msg = QString("Make sure you entered a correct and supported video file path: ") + videoPath;
            QMessageBox::critical(this, "Video Error", msg);
            return false;
        }
    }
    return true;
}

void MainWindow::loadCalibrationData(const QString& filePath)
{
    FileStorage stereoData(filePath.toStdString(), FileStorage::READ);
    if (!stereoData.isOpened()) {
        std::cerr << "Could not open the camera data file!" << std::endl;
        return;
    }
    stereoData["KL"] >> st.KL;
    stereoData["KR"] >> st.KR;
    stereoData["DL"] >> st.DL;
    stereoData["DR"] >> st.DR;
    stereoData["R"] >> st.R;
    stereoData["T"] >> st.T;
    stereoData["RL"] >> st.RL;
    stereoData["RR"] >> st.RR;
    stereoData["PL"] >> st.PL;
    stereoData["PR"] >> st.PR;
    stereoData["Q"] >> st.Q;
}

Mat MainWindow::rectifyImage(const Mat& frameInput, Camera camera)
{
    Mat mapx, mapy;
    switch (camera) {
    case Right:
        initUndistortRectifyMap(st.KR, st.DR, st.RR, st.PR, frameInput.size(), CV_32F, mapx, mapy);
        break;
    default:
        initUndistortRectifyMap(st.KL, st.DL, st.RL, st.PL, frameInput.size(), CV_32F, mapx, mapy);
        break;
    }

    Mat frame_rect;
    remap(frameInput, frame_rect, mapx, mapy, INTER_LINEAR);
    return frame_rect;
}

cv::Mat MainWindow::equalizeImage(const cv::Mat& frameInput)
{
    // make two camera pictures lok similar (brightness, white balance)
    Mat ret;
    Mat channels[3];
    cv::cvtColor(frameInput, ret, CV_BGR2YCrCb);
    cv::split(ret, channels);
    cv::equalizeHist(channels[0], channels[0]);
    cv::merge(channels, 3, ret);
    cv::cvtColor(ret, ret, CV_YCrCb2BGR);
    return ret;
}

double MainWindow::calDistance(const SURFData& surfData, Rect roi, const cv::Mat& Q, list<cv::DMatch>& matchesList)
{
    std::vector<double> rets;

    auto it = matchesList.begin();
    while (it != matchesList.end()) {
        auto leftPoint  = surfData.keyPointL[it->queryIdx];
        auto rightPoint = surfData.keyPointR[it->trainIdx];
        if (abs(leftPoint.pt.y - rightPoint.pt.y) > TESTSSGBVar5) {
            it = matchesList.erase(it);
            continue;
        } else if (roi.contains(leftPoint.pt)) {
            std::cout << "found feature points matcher!" << std::endl;
            std::cout << "coordinate Left x: " << leftPoint.pt.x << "coordinate Left y: " << leftPoint.pt.y << "coordinate Right x: " << rightPoint.pt.x << "coordinate Right y: " << rightPoint.pt.y << std::endl;
            std::cout << "disparity y: " << leftPoint.pt.y - rightPoint.pt.y << std::endl;
            std::cout << "disparity x: " << leftPoint.pt.x - rightPoint.pt.x << std::endl;
            double focal    = Q.at<double>(2, 3);
            double cx       = abs(Q.at<double>(0, 3));
            double cy       = abs(Q.at<double>(1, 3));
            double baseline = abs(1. / Q.at<double>(3, 2));
            double z        = focal * baseline / abs(leftPoint.pt.x - rightPoint.pt.x);
            double x        = (leftPoint.pt.x - cx) * z / focal;
            double y        = (leftPoint.pt.y - cy) * z / focal;
            double dis      = sqrt(x * x + y * y + z * z);
            std::cout << "World coordinate x: " << x << " y: " << y << " z: " << z << " distance: " << dis << " mm." << std::endl;
            rets.push_back(dis);
            it = matchesList.erase(it);
        } else {
            it++;
        }
    }

    double ret = 0;
    if (rets.empty()) {
        return ret;
    }
    // return the median value
    std::nth_element(rets.begin(), rets.begin() + rets.size() / 2, rets.end());
    ret = rets[rets.size() / 2];
    ret = ret / 1000.0; // convert to meter
    return ret;
}

void MainWindow::objectDetection(const cv::Mat& frameInput, cv::Mat& frameOutput, const SURFData& surfData)
{
    std::vector<Rect> objs;
    auto              scaleFactor  = TESTSSGBVar1;
    auto              minNeighbors = TESTSSGBVar2;
    auto              flags        = 0;
    Size              minSize      = Size(TESTSSGBVar3, TESTSSGBVar3);
    Size              maxSize      = Size();

    // use DNN or lbp for detection
    objs = faceDetector.detect(frameInput);

    // use haar or lbp for detection
    //cascade.detectMultiScale(frameInput, objs, scaleFactor, minNeighbors, flags, minSize, maxSize);

    // use Hog for detection
    // defaultHog.detectMultiScale(frameInput, objs, 0, Size(8, 8), Size(0, 0), 0.6, 1);
    //    std::vector<Rect> objsFiltered;
    //    for (unsigned int i = 0; i < objs.size(); i++) {
    //        Rect r = objs[i];
    //        for (unsigned int j = i; j < objs.size(); j++) {
    //            //filter out overlapping rectangles
    //            if (j != i) {
    //                auto iRect         = r;
    //                auto jRect         = objs[j];
    //                auto intersectRect = (iRect & jRect);
    //                if (intersectRect.area() >= iRect.area() * 0.75)
    //                    break;
    //            }
    //            if (j == objs.size()) {
    //                objsFiltered.push_back(r);
    //            }
    //        }
    //    }
    //    objs = objsFiltered;

    // adapter of matches container for calculation of distence
    list<cv::DMatch> matchesList(surfData.matches.cbegin(), surfData.matches.cend());
    for (const auto& obj : objs) {
        rectangle(frameOutput, obj, CV_RGB(0, 255, 0), 2);
        auto dis = calDistance(surfData, obj, st.Q, matchesList);
        // if no feature points then expand rect with 10%
        if (dis < 0.1) {
            double expand = TESTSSGBVar4;
            auto   xNew   = obj.x - obj.width * expand / 2.;
            auto   yNew   = obj.y - obj.height * expand / 2.;
            Rect   objNew(xNew, yNew, obj.width * (1. + expand), obj.height * (1. + expand));
            rectangle(frameOutput, objNew, CV_RGB(255, 0, 0), 2);
            dis = calDistance(surfData, objNew, st.Q, matchesList);
        }
        std::stringstream stream;
        stream << std::fixed << std::setprecision(1) << dis;
        auto  disStr = dis < 0.1 ? std::string() : stream.str();
        Point textOrigin(obj.x, obj.y + obj.height);
        putText(frameOutput, disStr, textOrigin, FONT_HERSHEY_PLAIN, 3, CV_RGB(0, 255, 0), 3, 8);
    }
}

SURFData MainWindow::stereoSURF(const cv::Mat& frameInputLeft, const cv::Mat& frameInputRight, cv::Mat& frameMatch)
{
    double                         hessianThreshold = TESTSSGBVar6;
    int                            nOctaves         = TESTSSGBVar7;
    int                            nOctaveLayers    = TESTSSGBVar8;
    bool                           extended         = true;
    bool                           upright          = false;
    cv::Ptr<cv::xfeatures2d::SURF> surf             = cv::xfeatures2d::SURF::create(hessianThreshold, nOctaves, nOctaveLayers, extended, upright);

    // detect and match feature points
    std::vector<cv::KeyPoint> keyPointL, keyPointR;
    cv::Mat                   despL, despR;
    surf->detectAndCompute(frameInputLeft, cv::Mat(), keyPointL, despL);
    surf->detectAndCompute(frameInputRight, cv::Mat(), keyPointR, despR);
    std::vector<cv::DMatch> matches;

    // since we use flannBased method, we need convert data type
    if (despL.type() != CV_32F || despR.type() != CV_32F) {
        despL.convertTo(despL, CV_32F);
        despR.convertTo(despR, CV_32F);
    }
    cv::Ptr<cv::DescriptorMatcher> matcher = cv::DescriptorMatcher::create(DescriptorMatcher::FLANNBASED);
    matcher->match(despL, despR, matches);

    // calculate max distence (difference)
    double maxDist = 0;
    for (int i = 0; i < despL.rows; i++) {
        double dist = matches[i].distance;
        if (dist > maxDist)
            maxDist = dist;
    }

    // select good matched feature points
    auto                    selector = TESTSSGBVar9;
    std::vector<cv::DMatch> goodMatches;
    for (int i = 0; i < despL.rows; i++) {
        if (matches[i].distance < selector * maxDist) {
            goodMatches.push_back(matches[i]);
        }
    }

    cv::drawMatches(frameInputLeft, keyPointL, frameInputRight, keyPointR, goodMatches, frameMatch);

    SURFData surfData;
    surfData.keyPointL = keyPointL;
    surfData.keyPointR = keyPointR;
    surfData.matches   = goodMatches;

    return surfData;
}

void MainWindow::showImage(const cv::Mat& frameInput, Camera camera)
{
    QGraphicsPixmapItem* pixmapPtr   = nullptr;
    QGraphicsView*       graphicView = nullptr;
    QImage::Format       format      = QImage::Format_RGB888;

    switch (camera) {
    case Right:
        pixmapPtr   = &pixmapRight;
        graphicView = ui->graphicsViewRight;
        break;
    case Left:
        pixmapPtr   = &pixmapLeft;
        graphicView = ui->graphicsViewLeft;
        break;
    default:
        pixmapPtr   = &pixmapTest;
        graphicView = ui->graphicsViewTEST;
        //format      = QImage::Format_Indexed8;
        break;
    }
    QImage qimg(frameInput.data, frameInput.cols, frameInput.rows, frameInput.step, format);
    pixmapPtr->setPixmap(QPixmap::fromImage(qimg));
    graphicView->fitInView(pixmapPtr, Qt::KeepAspectRatio);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::onStartBtnPressed()
{
    // open camera
    if (videoLeft.isOpened() || videoRight.isOpened()) {
        ui->startBtn->setText("Start");
        videoLeft.release();
        videoRight.release();
        return;
    }
    auto leftOpen  = openCamera(ui->videoEditLeft->text(), videoLeft);
    auto rightOpen = openCamera(ui->videoEditRight->text(), videoRight);
    if (!leftOpen || !rightOpen)
        return;
    ui->startBtn->setText("Stop");

    auto skip = 0;
    while (videoLeft.isOpened() && videoRight.isOpened()) {
        Mat frameL, frameGrayL, frameRgbL, frameR, frameGrayR, frameRgbR;
        videoLeft >> frameL;
        videoRight >> frameR;

        skip++;
        if (skip != TESTSSGBVar10) { // skipFrame
            continue;
        } else {
            skip = 0;
        }

        // image rectification
        frameL = rectifyImage(frameL, Camera::Left);
        frameR = rectifyImage(frameR, Camera::Right);

        // change color model, gray for detection, rgb for display
        //cvtColor(frameL, frameGrayL, CV_BGR2GRAY);
        cvtColor(frameL, frameRgbL, CV_BGR2RGB);
        //cvtColor(frameR, frameGrayR, CV_BGR2GRAY);
        cvtColor(frameR, frameRgbR, CV_BGR2RGB);

        // process feature points
        Mat  frameMatch;
        auto surfData = stereoSURF(frameL, frameR, frameMatch);

        // detection
        //objectDetection(frameGrayL, frameRgbL, surfData);
        objectDetection(frameL, frameRgbL, surfData);

        //transform to QImage
        showImage(frameRgbL, Camera::Left);
        showImage(frameRgbR, Camera::Right);
        cvtColor(frameMatch, frameMatch, CV_BGR2RGB);
        showImage(frameMatch, Camera::TEST);

        qApp->processEvents();
    }

    ui->startBtn->setText("Start");
}

void MainWindow::closeEvent(QCloseEvent* event)
{
    if (videoLeft.isOpened() || videoRight.isOpened()) {
        QMessageBox::warning(this,
                             "Warning",
                             "Stop the video before closing the application!");
        event->ignore();
    } else {
        event->accept();
    }
}
