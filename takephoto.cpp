#include <QDebug>
#include <QDir>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace std;
using namespace cv;

int x = 0;

int main(int argc, char* argv[])
{
    const QString imageDirRelPath("../image");
    if (!QDir(imageDirRelPath).exists()) {
        QDir().mkdir(imageDirRelPath);
    }
    QDir directory(imageDirRelPath);

    const QString leftVideoPath(argv[1]);
    const QString rightVideoPath(argv[2]);
    const QString extension(".jpg");
    const QString leftImageNamePrifx("left");
    const QString rightImageNamePrifx("right");
    const auto    dirAbsolutePath = directory.absolutePath();

    VideoCapture leftCap(leftVideoPath.toStdString());
    VideoCapture rightCap(rightVideoPath.toStdString());
    Mat          leftImage, rightImage;

    auto imageIndex = 0;
    while (1) {
        leftCap >> leftImage;
        rightCap >> rightImage;
        imshow("Left image", leftImage);
        imshow("Right image", rightImage);
        if (waitKey(30) > 0) {
            imageIndex++;
            auto leftImageFile  = dirAbsolutePath + QDir::separator() + leftImageNamePrifx + QString::number(imageIndex) + extension;
            auto rightImageFile = dirAbsolutePath + QDir::separator() + rightImageNamePrifx + QString::number(imageIndex) + extension;
            qInfo() << "Saving image pair " << imageIndex;
            imwrite(leftImageFile.toStdString(), leftImage);
            imwrite(rightImageFile.toStdString(), rightImage);
        }
    }
    return 0;
}
