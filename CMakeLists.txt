project(myproject LANGUAGES CXX)

cmake_minimum_required(VERSION 3.8.0)

set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

set(CMAKE_CXX_STANDARD 14)

set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(UNIX_COMMON_COMPILE_OPTIONS -Wall -Wextra -pedantic -Wcast-qual
                                -Wdisabled-optimization -Wformat -Winit-self -Woverloaded-virtual
                                -Wredundant-decls -Wshadow -Wundef -Wno-unused -Wno-variadic-macros
                                -Wfloat-equal -Werror -Wold-style-cast)
set(UNIX_HOST_COMPILE_OPTIONS ${UNIX_COMMON_COMPILE_OPTIONS}
                              -Wcast-align -Wctor-dtor-privacy -Wmissing-include-dirs
                              -Wsign-promo -Wno-error=missing-include-dirs)
set(UNIX_DEVICE_COMPILE_OPTIONS ${UNIX_COMMON_COMPILE_OPTIONS}
                                -Wno-error=sign-promo -Wno-error=delete-non-virtual-dtor)

if (CMAKE_CXX_COMPILER_VERSION VERSION_GREATER_EQUAL 9.1)
    set(UNIX_COMMON_COMPILE_OPTIONS "${UNIX_COMMON_COMPILE_OPTIONS} -Wno-deprecated-copy")
endif()

set(COMPILE_OPTIONS "")
if (MSVC)
    # not supported...
else (MSVC)
    if (NOT CMAKE_CROSSCOMPILING)
        set(COMPILE_OPTIONS ${UNIX_HOST_COMPILE_OPTIONS})
    else()
        set(COMPILE_OPTIONS ${UNIX_DEVICE_COMPILE_OPTIONS})
    endif()

    if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
        set(COMPILE_OPTIONS ${COMPILE_OPTIONS} -Wno-psabi)
    endif()
endif()

# Turn on automatic invocation of the MOC, UIC & RCC
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTORCC ON)

include_directories(${CMAKE_SOURCE_DIR})

find_package(Qt5 COMPONENTS Core Widgets REQUIRED)
find_package(OpenCV REQUIRED )

include_directories( ${OpenCV_INCLUDE_DIRS} )

add_executable(VideoStream 
    main.cpp
    mainwindow.cpp
    FaceDetector.cpp
    mainwindow.ui
)

target_link_libraries(VideoStream 
    Qt5::Widgets
    ${OpenCV_LIBS}
)

add_executable(calibration 
    calibration.cpp
)

add_executable(takephoto 
    takephoto.cpp
)

target_link_libraries(calibration 
    Qt5::Core
    ${OpenCV_LIBS}
)

target_link_libraries(takephoto 
    Qt5::Core
    ${OpenCV_LIBS}
)

install(TARGETS takephoto calibration VideoStream 
    DESTINATION ${CMAKE_INSTALL_BINDIR}
)

