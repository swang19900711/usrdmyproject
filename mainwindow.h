#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QCloseEvent>
#include <QDebug>
#include <QGraphicsPixmapItem>
#include <QGraphicsScene>
#include <QImage>
#include <QMainWindow>
#include <QMessageBox>
#include <QPixmap>

#include "FaceDetector.h"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/imgproc/types_c.h"
#include "opencv2/opencv.hpp"
#include <opencv2/ximgproc/disparity_filter.hpp>
//#include "opencv2/ximgproc/segmentation.hpp"

namespace Ui {
class MainWindow;
}

enum Camera
{
    Left,
    Right,
    TEST
};

struct StereoData
{
    cv::Mat   DL; // distortion coefficients of left camera
    cv::Mat   DR; // distortion coefficients of right camera
    cv::Mat   KL; // intrinsic matrix of left camera
    cv::Mat   KR; // intrinsic matrix of right camera
    cv::Mat   R;  // rotation from the left to the right camera
    cv::Vec3d T;  //  translation from the left to the right camera
    cv::Mat   RL; // rectification transform for the left camera
    cv::Mat   RR; // rectification transform for the right camera
    cv::Mat   PL; // projection matrix in the new rectified coordinate system for the left camera
    cv::Mat   PR; // projection matrix in the new rectified coordinate system for the right camera
    cv::Mat   E;  // essential matrix
    cv::Mat   F;  // fundamental matrix
    cv::Mat   Q;  // disparity-to-depth mapping matrix
};

struct SURFData
{
    std::vector<cv::KeyPoint> keyPointL, keyPointR;
    std::vector<cv::DMatch>   matches;
};

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget* parent = 0);
    ~MainWindow();

protected:
    void closeEvent(QCloseEvent* event);

private slots:
    void onStartBtnPressed();
    void TESTVarUpdate();

private:
    void   TESTVarSetDefault();
    double TESTSSGBVar1  = 1;
    int    TESTSSGBVar2  = 1;
    int    TESTSSGBVar3  = 1;
    double TESTSSGBVar4  = 1;
    int    TESTSSGBVar5  = 1;
    int    TESTSSGBVar6  = 1;
    int    TESTSSGBVar7  = 1;
    int    TESTSSGBVar8  = 1;
    double TESTSSGBVar9  = 1;
    int    TESTSSGBVar10 = 1;

private:
    bool    openCamera(const QString& videoPath, cv::VideoCapture& videoCap);
    void    loadCalibrationData(const QString& filePath);
    cv::Mat rectifyImage(const cv::Mat& frameInput, Camera camera);
    cv::Mat equalizeImage(const cv::Mat& frameInput);
    void    objectDetection(const cv::Mat& frameInput, cv::Mat& frameOutput, const SURFData& surfData);
    double  calDistance(const SURFData& surfData, cv::Rect roi, const cv::Mat& Q, std::list<cv::DMatch>& matchesList);
    void    showImage(const cv::Mat& frameInput, Camera camera);

    SURFData stereoSURF(const cv::Mat& frameInputLeft, const cv::Mat& frameInputRight, cv::Mat& frameMatch);

    Ui::MainWindow*       ui;
    StereoData            st;
    cv::CascadeClassifier cascade;    // used for detection
    cv::HOGDescriptor     defaultHog; // used for detection
    FaceDetector          faceDetector;

    QGraphicsPixmapItem pixmapLeft;
    cv::VideoCapture    videoLeft;

    QGraphicsPixmapItem pixmapRight;
    cv::VideoCapture    videoRight;

    QGraphicsPixmapItem pixmapTest;
    int                 skipFrame; // for the acceleration, we do not process all frame but process one frame every skipFrame frames
};

#endif // MAINWINDOW_H
